package resume

import (
  "log"
  "appengine"
  "appengine/datastore"
  "net/url"
  "github.com/GoogleCloudPlatform/go-endpoints/endpoints"
)

type JobsAPI struct {}

var jobs = []Job{
  {
    "Sysadmin/Programmer/DevOps",
    "Workjoy Argentina S.A.",
    "2008-08-01",
    "current",
    "Infrastructure designer/implementator,Build systems to control and improve response times on sysadmin tasks, develop and deploy management system.",
  },{
    "Programmer",
    "Cirope S.A.",
    "2013-01-01",
    "2013-05-31",
    "Ruby Programmer, using Rails framework",
  },{
    "Computer Technician",
    "Norfix S.A.",
    "2008-04-01",
    "2008-08-01",
    "Tech support, Pc repairment, Networking",
  },{
    "Computer Technician",
    "CyberPoints",
    "2007-01-01",
    "2008-08-01",
    "Pc repairment, Network administration",
  },{
    "Sysadmin",
    "S.T.I. Informatica",
    "2005-08-01",
    "2008-02-01",
    "Network administration",
  },
}

var studies = []Job{
  {
    "Systems engineering",
    "FRM UTN",
    "2010-01-01",
    "2013-01-01",
    "2nd year",
  },{
    "M101P",
    "MongoDB University",
    "2013-01-01",
    "2013-12-01",
    "MongoDB for Python developers",
  },{
    "General Game Playing",
    "Coursera",
    "2013-01-01",
    "2013-12-01",
    "Ruby on rails course",
  },{
    "M102",
    "MongoDB University",
    "2013-01-01",
    "2013-12-01",
    "MongoDB for system administrators",
  },{
    "Ruby on Rails",
    "UTN FRM",
    "2011-01-01",
    "2011-12-01",
    "Ruby on rails course",
  },{
    "ESR",
    "Proydesa",
    "2008-01-01",
    "2009-01-01",
    "Enterprise Security \u0026 Risk",
  },{
    "CCNA Cisco Certified Associate",
    "ISRI",
    "2005-01-01",
    "2007-01-01",
    "CCNA / Networking",
  },{
    "Basic Programming and Databases",
    "Institute Saavedra",
    "2004-01-01",
    "2005-01-01",
    "Basics of C, Php, ASP, javascript, MSSQL, MySQL",
  },{
    "High School's degree, informatic",
    "E.P.E.T. Nº 4",
    "1999-01-01",
    "2003-01-01",
    "Professional Informatic",
  },
}

type Job struct {
  Title string `json:"title"`
  Institution string `json:"institution"`
  From string `json:"from"`
  To string `json:"to"`
  Description string `json:"description"`
}

type Jobs struct {
  Jobs []Job
}

type Stydies struct {
  Jobs []Job
}

func checkReferer(c endpoints.Context) error {
  if appengine.IsDevAppServer() {
    return nil
  }

  r := c.HTTPRequest().Referer()
  u, err := url.Parse(r)
  if err != nil {
    c.Infof("malformed referer detected: %q", r)
    return endpoints.NewUnauthorizedError("couldn't extract domain from referer")
  }

  if u.Host != appengine.AppID(c)+".appspot.com" {
    c.Infof("unauthorized referer detected: %q", r)
    return endpoints.NewUnauthorizedError("referer unauthorized")
  }
  return nil
}

func loadData(c endpoints.Context) error {
  // datastore.DeleteMulti(c, nil)
  // for _, job := range jobs {
  //   k := datastore.NewIncompleteKey(c, "Job", nil)
  //   t := &Job{Title: job.Title, Institution: job.Institution, From: job.From, To: job.To , Description: job.Description }
  //   log.Printf("%+v", t)
  //   k, err := datastore.Put(c, k, t)
  //   if err != nil {
  //     return err
  //   }
  // }

  return nil
}

func (JobsAPI) List(c endpoints.Context) (*Jobs, error) {
  err := loadData(c)
  if err != nil {
    log.Fatal(err)
  }

  if err := checkReferer(c); err != nil {
    return nil, err
  }

  jobs := []Job{}
  _, err = datastore.NewQuery("Job").GetAll(c, &jobs)
  if err  != nil {
    return nil, err
  }

  log.Printf("%+v", jobs)
  return &Jobs{jobs}, nil
}

func init() {

  api, err := endpoints.RegisterService(JobsAPI{}, "jobs", "v1", "jobs API", true)
  if err != nil {
    log.Fatal(err)
  }

  // adapt the name, method, and path for each method.
  info := api.MethodByName("List").Info()
  // info.Name, info.HTTPMethod, info.Path = "getJobs", "GET", "jobs"
  info.Name = "getJobs"


  endpoints.HandleHTTP()
}
